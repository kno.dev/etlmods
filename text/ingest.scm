;;; -*- Mode: Scheme; Character-encoding: utf-8; -*-
;;; Copyright (C) 2005-2020 beingmeta, inc.  All rights reserved.
;;; Copyright (C) 2020-2024 Kenneth Haase (ken.haase@alum.mit.edu).

(in-module 'etl/text/ingest)

(module-export! '{ingest-textfile})

(use-module '{texttools fifo reflection etl/ingest})

(define default-separator #\tab)
(define default-terminator #\newline)
(define default-escape #\\)
(define default-comment #\#)
(define default-quotevals #f)

(define (quotevals? opts) (getopt opts 'quotevals default-quotevals))

(define (open-path path)
  (cond ((file-exists? path) (open-input-file path))
        ((source-exists? path) (open-input-string (source->string path)))
        (else (irritant path |BadInputFile|))))

(define (simple-char? s)
  (or (not s) (character? s) (and (string? s) (= (length s) 1))))

(define (ingest-textfile path (opts #f) cols (fields) (output))
  "Reads a text file"
  ;; Handle either <opts,cols> or <cols,opts> arguments
  (when (vector? opts)
    (when (and (bound? cols) (opts? cols))
      (let ((use-cols opts))
        (set! opts cols)
        (set! cols use-cols))))
  (default! cols #f)
  (default! fields (getopt opts 'fields opts))
  (default! output (getopt opts 'output #f))
  (local ingest-opts #[])
  (set! opts (cons ingest-opts opts))
  (unless cols
    (set! cols (extract-column-headers (open-path path) ingest-opts opts)))
  (let* ((port (open-path path))
         (input (cond ((getopt opts 'widths)
                       (cons columnize (vector port opts (get opts 'widths))))
                      ((getopt opts 'spans)
                       (cons columnize (vector port opts (get opts 'spans))))
                      ((and (getopt opts 'separator)
                            (simple-char? (getopt opts 'separator))
                            (simple-char? (getopt opts 'escape))
                            (simple-char? (getopt opts 'terminator)))
                       (cons readcols
                             (vector port opts (getopt opts 'separator default-separator)
                                     (getopt opts 'terminator default-terminator)
                                     (getopt opts 'escape (if (quotevals? opts) #f default-escape))
                                     (getopt opts 'comment #\#)
                                     (quotevals? opts))))
                      (else ))))
    (ingest-frames input output opts cols)))

(define (extract-column-headers port local opts)
  (let* ((comment (getopt opts 'comment default-comment))
         (terminator (getopt opts 'terminator default-terminator))
         (separator (getopt opts 'separator))
         (record (read-record port terminator)))
    (while (and (string? record) comment (string-starts-with? record comment))
      (set! record (read-record port terminator)))
    (unless separator (set! separator (guess-separator record)))
    (if separator
        (map string->symbol (map trim-spaces (->vector (text/slice record separator))))
        (irritant record |NoSeparator|))))

(define (sep-count string sep)
  (length (gather->list string sep)))

(define (guess-separator record)
  (let ((best-sep #\tab)
        (best-count (sep-count record #\tab)))
    (doseq (sep '("|" "," ";" ":"))
      (let ((count (sep-count record sep)))
        (when (> count sep-count)
          (set! best-sep sep)
          (set! best-count count))))
    best-sep))

