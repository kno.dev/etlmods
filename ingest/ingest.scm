;;; -*- Mode: Scheme; Character-encoding: utf-8; -*-
;;; Copyright (C) 2020-2024 Kenneth Haase (ken.haase@alum.mit.edu).

(in-module 'etl/ingest)

(use-module '{texttools varconfig logger fifo})

(module-export! '{ingest-frames ingest-frames-loop})

(define default-valmap #f)
(varconfig! etl:valmap default-valmap)

(define (kno/map-value v spec) v)

;;; The conversion loop

(define (ingest-frames input output opts (fields) (cols) (sample))
  (default! fields (getopt opts 'fields opts))
  (default! cols (getopt opts 'columns))
  (local readfn (get-readfn input opts)
         readargs (get-readargs input opts)
         outfn (get-outfn input opts)
         outargs (get-outargs input opts)
         slotids (make-vector (length cols))
         colopts (make-vector (length cols))
         colmaps (make-vector (length cols)))
  (let* ((template (create-template cols opts fields slotids colopts colmaps)))
    (ingest-frames-loop input output opts cols
                        readfn readargs outfn outargs
                        slotids colmaps colopts
                        template #f)))

(define (ingest-frames-loop input output opts cols
                            readfn readargs outfn outargs
                            slotids colmaps colopts
                            template norm)
  (let ((record (cond ((not readargs) (readfn))
                      ((not (pair? readargs)) (readfn readargs))
                      (else (apply readfn readargs))))
        (limit (getopt opts 'limit #f))
        (skip (getopt opts 'skip 0))
        (results {})
        (count 0))
    (while (and (exists? record) record (not (eof? record)))
      (when (>= count skip)
        (if outfn
            (handle-output (convert-row record template norm slotids colmaps) outfn output outargs)
            (set+! results (convert-row record template norm slotids colmaps))))
      (set! count (1+ count))
      (set! record
        (cond ((and limit (> count limit)) #f)
              ((or (fail? readargs) (not readargs)) (readfn))
              ((not (pair? readargs)) (readfn readargs))
              (else (apply readfn readargs)))))
    (if outfn output results)))

;;; Converting individual rows

(define (convert-row cells template norm slotids colmaps)
  (let ((converted (forseq (cell cells i) (kno/map-value (elt colmaps i) cell))))
    (if template
        (->sample converted template)
        converted)))

;;;; Initializing column info

(define (create-template cols opts fields slotids colopts colmaps)
  (local default-valmap (getopt opts 'valmap))
  (local id (getopt opts 'schema #f))
  (local info #[])
  (doseq (colspec cols i)
    (let ((slotid #f)
          (fieldinfo #f)
          (colinfo #f)
          (slotid #f)
          (valmap #f))
      (cond ((symbol? colspec)
             (set! slotid colspec))
            ((and (pair? colspec) (slotid? (car colspec)))
             (set! slotid (car colspec))
             (set! colinfo (cdr colspec)))
            ((and (table? colspec) (slotid? (getopt colspec 'slotid #f)))
             (set! colinfo colspec)
             (set! slotid (getopt colspec 'slotid #f)))
            (else (irritant colspec |BadColumInfo|)))
      (set! fieldinfo (getopt fields slotid #f))
      (let* ((opts (opt+ colinfo fieldinfo))
             (valmap (getopt opts 'valmap valmap))
             (colopts (opt+ `#[slotid ,slotid colno ,i] opts)))
        (vector-set! slotids i slotid)
        (vector-set! colopts i colopts)
        (vector-set! colmaps i valmap)
        (store! info slotid colopts))))
  (->template info id))

;;;; Handling input and output

(define (handle-output result outfn output outarg)
  (cond ((and output (pair? outarg)) (apply outfn result outarg))
        ((and output outarg)         (outfn result output outarg))
        (output (outfn result output))
        (else (outfn result))))

(define (get-readfn input opts)
  (cond ((fifo? input) fifo/pop)
        ((and (pair? input) (applicable? (car input)))
         (car input))
        ((port? input) (get-port-readfn input opts))
        ((and (string? input)
              (or (multiline-string? input)
                  (file-exists? input)
                  (source-exists? input)))
         (get-port-readfn input opts))
        (else (irritant input |BadRowInput| "opts=" opts))))

(define (get-readargs input opts)
  (cond ((fifo? input) input)
        ((and (pair? input) (applicable? (car input)))
         (cdr input))
        ((string? input)
         (cond ((multiline-string? input) (open-input-string input))
               ((file-exists? input) (open-input-file input))
               ((source-exists? input) (open-input-string (source->string input)))
               (else (irritant input |BadInput|))))
        ((input-port? input) (get-port-readargs input opts))
        (else (irritant input |BadRowInput| "opts=" opts))))

(define (get-port-readfn port opts)
  (cond ((getopt opts 'separator) readcols)
        ((getopt opts 'widths) columnize)
        ((getopt opts 'spans) columnize)
        (else (irritant port |UnknownFileFormat| "with opts: " opts))))

(define (get-port-readargs port opts)
  (cond ((getopt opts 'separator)
         (vector port opts (getopt opts 'separator) (getopt opts 'terminator #\newline)
                 (getopt opts 'escape #\backslash) (getopt opts 'comment #\#)
                 #t))
        ((getopt opts 'widths) (vector port opts (getopt opts 'widths)))
        ((getopt opts 'spans) (vector port opts (getopt opts 'spans)))
        (else (irritant port |UnknownFileFormat| "with opts: " opts))))

(define (get-outfn output opts)
  (cond ((fifo? output) fifo/push!)
        ((and (pair? output) (applicable? (car output)))
         (if (and (pair? (cdr output))
                  (not (pair? (cadr output)))
                  (empty-list? (cddr output)))
             (cadr output)
             (cdr output)))
        (else (irritant output |BadFrameOutput| "opts=" opts))))

(define (get-outargs output opts)
  (cond ((fifo? output) output)
        ((and (pair? output) (applicable? (car output)))
         (if (and (pair? (cdr output))
                  (not (pair? (cadr output)))
                  (empty-list? (cddr output)))
             (cadr output)
             (cdr output)))
        (else (irritant output |BadFrameOutput| "opts=" opts))))

