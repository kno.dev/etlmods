;;; -*- Mode: Scheme; Character-encoding: utf-8; -*-
;;; Copyright (C) 2020-2024 Kenneth Haase (ken.haase@alum.mit.edu).

(in-module 'etl/dataframes)

(use-module '{texttools varconfig logger fifo})

(define-once dataframe-info (make-hashtable))

;;;; Getting dataframe info

(define (df/info id (slotid #f))
  (try (if slotid
           (get dataframe-info (cons id slotid))
           (get dataframe-info id))
       )
  )

